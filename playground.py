import pandas as pd


def check_short_position_profit_or_loss(df, i, stop_loss, entry_point, profit_level, number_of_trading_days, ratio):
    target_level = entry_point - ratio * profit_level

    lists_of_highs = pd.Series(df[i + 1:i + number_of_trading_days + 1]['High'])
    lists_of_lows = pd.Series(df[i + 1:i + number_of_trading_days + 1]['Low'])

    high = lists_of_highs[lists_of_highs > stop_loss]
    low = lists_of_lows[lists_of_lows <= target_level]

    max_of_highs = max(lists_of_highs)
    min_of_lows = min(lists_of_lows)

    if target_level > min_of_lows:  # For successful cases, target_level must be met.

        if stop_loss >= max_of_highs:  # The first successful case, stop_loss is never be broken.
            return 1

        if high.index[0] > low.index[0]:
            # The second successful case, it meets the target_level first then break the stop_loss.
            return 1
        return 0

    else:
        return 0


def check_long_position_profit_or_loss(df, i, stop_loss, entry_point, profit_level, number_of_trading_days, ratio):
    target_level = entry_point + ratio * profit_level

    lists_of_highs = pd.Series(df[i + 1:i + number_of_trading_days + 1]['High'])
    lists_of_lows = pd.Series(df[i + 1:i + number_of_trading_days + 1]['Low'])

    high = lists_of_highs[lists_of_highs >= target_level]
    low = lists_of_lows[lists_of_lows < stop_loss]

    max_of_highs = max(lists_of_highs)
    min_of_lows = min(lists_of_lows)

    if target_level < max_of_highs:  # For successful cases, target_level must be met.

        if stop_loss <= min_of_lows:  # The first successful case, stop_loss is never be broken.
            return 1

        if high.index[0] < low.index[0]:
            # The second successful case, it meets the target_level first then break the stop_loss.
            return 1

        return 0

    else:
        return 0
