import time

import pandas as pd

from technicalanalysispy.candlestick import Candlestick
from technical_indicators import utilities
from performanceanalysispy.performance_matrices import PerformanceMatrices
from backtestutilitiespy.util import evaluation_dict, fetch_signal_details

from app.backtest_util import strategy_template, generate_backtest_code, strategy_dict


FILE_PATH = './datasets/HSIF_LiquidityCorrected(Non-adjusted).csv'
df = pd.read_csv(FILE_PATH)
candle = Candlestick(df=df)

instrument_code = 'HSIF'

conditions = [{'description': 'candle.is_gap_up(i)',
               'stop_loss': 'candle.l[i-1]',
               'entry_point': 'candle.c[i]',
               'profit_level': 'candle.h[i-1] - candle.l[i-1]',
               'no_of_bars': 21,
               'action': 'LONG'}]

code = generate_backtest_code(resolution='daily', no_of_eng_char=4, no_of_digits=8,
                              instrument_code=instrument_code, direction='LONG', timeframe='21D')

daily_pct_change = candle.c.pct_change()
signal = pd.Series(0, index=candle.df.index)
positions = pd.Series(0, index=candle.df.index)
pnl = pd.Series(0.0, index=candle.df.index)

# This one is for entry at close.
# conditions[0]['entry_point'].split('.')[1][0] == 'c' or conditions[0]['entry_point'].split('.')[1][0] == 'o'
if conditions[0]['entry_point'].split('.')[1][0] == 'c':
    pos = 0
    if conditions[0]['action'] == 'LONG':
        pos = 1
        timestamp = int(time.time())
        for i in range(10, candle.no_of_bars - conditions[0]['no_of_bars']):
            if eval(conditions[0]['description']):
                signal[i] = -1

                result, target_level, bars_afterwards, ha = \
                    utilities.check_sl_tl_position_pnl(direction=conditions[0]['action'], df=candle.df, i=i,
                                                       stop_loss=eval(conditions[0]['stop_loss']),
                                                       entry_point=eval(conditions[0]['entry_point']),
                                                       profit_level=eval(conditions[0]['profit_level']),
                                                       no_of_bars=conditions[0]['no_of_bars'], ratio=1)

                positions[i:i+bars_afterwards] += pos

                if result is None:
                    print(f'[ERROR] The direction is invalid.')

                elif result:
                    signal[i] = 1
                    # print(pnl[i])
                    for j in range(1, bars_afterwards):
                        if j == bars_afterwards or j == bars_afterwards - 1:
                            pnl[i + bars_afterwards] += (target_level - candle.c[i+bars_afterwards-1])\
                                                        / candle.c[i+bars_afterwards-1] - 1
                        pnl[i+j] += pos * daily_pct_change[i+j]

                else:
                    signal[i] = -1
                    for j in range(1, bars_afterwards):
                        if j == bars_afterwards or j == bars_afterwards - 1:
                            pnl[i + bars_afterwards] += (eval(conditions[0]['stop_loss']) -
                                                         candle.c[i + bars_afterwards - 1]) \
                                                        / candle.c[i + bars_afterwards - 1] - 1
                        pnl[i + j] += pos * daily_pct_change[i + j]

        pm = PerformanceMatrices(pnl=pnl)
        pm.performance_plot()
        # Save it into the database/dataframe.
        # performance_dict = \
        #     evaluation_dict(strategy_code='NA', ernest_ratio=0.0,
        #                     sharpe_ratio=pm.fetch_annualized_sharpe_ratio(),
        #                     annual_return=pm.fetch_annualized_return(),
        #                     sortino_ratio=pm.fetch_sortino_ratio(),
        #                     calmar_ratio=pm.fetch_calmar_ratio(),
        #                     maxi_drawdown=pm.fetch_maximum_drawdown(),
        #                     frequency=fetch_signal_details(signal=signal)[1],
        #                     win_rate=0, time_to_recover=0,
        #                     description=conditions[0]['description'],
        #                     last_backtest_time=timestamp)

    elif conditions[0]['action'] == 'SHORT':
        pos = -1
        timestamp = time.time()
        for i in range(10, candle.no_of_bars - conditions[0]['no_of_bars']):
            if eval(conditions[0]['description']):
                signal[i] = -1

                result, target_level, bars_afterwards, ha = \
                    utilities.check_sl_tl_position_pnl(direction=conditions[0]['action'], df=candle.df, i=i,
                                                       stop_loss=eval(conditions[0]['stop_loss']),
                                                       entry_point=eval(conditions[0]['entry_point']),
                                                       profit_level=eval(conditions[0]['profit_level']),
                                                       no_of_bars=conditions[0]['no_of_bars'], ratio=1)

                positions[i:i+bars_afterwards] += pos

                if result is None:
                    print(f'[ERROR] The direction is invalid.')

                elif result:
                    signal[i] = 1
                    for j in range(1, bars_afterwards):
                        if j == bars_afterwards or j == bars_afterwards - 1:
                            pnl[i+bars_afterwards] += (candle.c[i+bars_afterwards-1] - target_level)\
                                                        / candle.c[i+bars_afterwards-1] - 1
                        pnl[i+j] += pos * daily_pct_change[i+j]

                else:
                    signal[i] = -1
                    for j in range(1, bars_afterwards):
                        if j == bars_afterwards or j == bars_afterwards - 1:
                            pnl[i + bars_afterwards] += (candle.c[i + bars_afterwards - 1] -
                                                         eval(conditions[0]['stop_loss'])) \
                                                        / candle.c[i + bars_afterwards - 1] - 1
                        pnl[i + j] += pos * daily_pct_change[i + j]

        pm = PerformanceMatrices(pnl=pnl)
        pm.performance_plot()

    else:
        print(f'[ERROR] The direction can either only be LONG or SHORT.')


