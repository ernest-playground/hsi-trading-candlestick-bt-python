import pandas as pd

from technicalanalysispy.candlestick import Candlestick
from technical_indicators import utilities
from app.backtest_util import strategy_template, generate_backtest_code, strategy_dict


FILE_PATH = './datasets/HSIF_LiquidityCorrected(Non-adjusted).csv'

# Check if it is saved in Database in advance (by description).
# If no, call generate_backtest_code, also check if the generated code is accidentially the same as any in DB.
# If no, save it into DB, else, do it again.

# If yes, just maintain it and directly update to DB.
# Key: backtest_code_description_timestamp(last backtest timestamp)

df = pd.read_csv(FILE_PATH)
candle = Candlestick(df=df)

# eval(), literal_eval()

instrument_code = 'HSIF'

# Retrieve from a text file.
# strategy_template()

conditions = [{'description': 'candle.is_gap_down(i)',
               'stop_loss': 'candle.l[i]',
               'entry_point': 'candle.c[i]',
               'profit_level': 'candle.c[i] - candle.l[i]',
               'no_of_bars': 21,
               'action': 'LONG'},
              "", ""]

# ====================================
code = generate_backtest_code(resolution='daily', no_of_eng_char=4, no_of_digits=8,
                              instrument_code=instrument_code, direction='SHORT', timeframe='21D')
# ====================================

daily_pct_change = candle.c.pct_change()
signal = pd.Series(0, index=candle.df.index)
positions = pd.Series(0, index=candle.df.index)
pnl = pd.Series(0.0, index=candle.df.index)
# ====================================
pos = 0
if conditions[0]['action'] == 'LONG':
    pos = 1
elif conditions[0]['action'] == 'SHORT':
    pos = -1
else:
    print(f'[ERROR] The direction can either only be LONG or SHORT.')
# ====================================

# This one is for entry at close.
# conditions[0]['entry_point'].split('.')[1][0] == 'c' or conditions[0]['entry_point'].split('.')[1][0] == 'o'

# signal: 1/-1 -> has trading signal. signal: 1 -> successful signal.
for i in range(5, candle.no_of_bars - conditions[0]['no_of_bars']):
    # conditions[]['description']
    if candle.is_gap_down(i):
        signal[i] = -1
        # conditions[]['action'], conditions[]['stop_loss'], conditions[]['entry_point'],
        # conditions[]['profit_level'], conditions[]['no_of_bars']
        result, bars_afterwards, pnl = \
            utilities.check_sl_tl_position_pnl(direction=conditions[0]['action'], df=candle.df, i=i,
                                               stop_loss=candle.h[i-1], entry_point=candle.c[i],
                                               profit_level=candle.h[i-1]-candle.l[i-1], no_of_bars=21, ratio=1)
        # LONG
        # target_level = entry_point + profit_level * ratio
        positions[i:i + bars_afterwards] += pos

        # LONG
        # Count how many trades & successful.
        if result:
            signal[i] = 1
            for j in range(1, bars_afterwards):
                if j == bars_afterwards - 1:
                    pnl[i + bars_afterwards] = (target_level - candle.c[i+bars_afterwards-1])/candle.c[i+bars_afterwards-1] - 1
                pnl[i + j] = pos * daily_pct_change[i + j]
        else:
            signal[i] = -1
            for j in range(1, bars_afterwards):
                if j == bars_afterwards:
                    pnl[i+bars_afterwards] = (stop_loss - candle.c[i+bars_afterwards-1])/candle.c[i+bars_afterwards-1] - 1
                pnl[i+j] = pos * daily_pct_change[i+j]

