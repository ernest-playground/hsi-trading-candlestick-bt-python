import pandas as pd

from technicalanalysispy.candlestick import Candlestick


class Ernest:

    def __init__(self, data_path, fmt='csv'):
        if fmt == 'csv':
            self._df = pd.read_csv(data_path)
        elif fmt == 'xlsx':
            self._df = pd.read_excel(data_path)
        else:
            print(f'The format is invalid.')

