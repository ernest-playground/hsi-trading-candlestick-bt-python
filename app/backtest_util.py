import string
import random
import re

import pandas as pd


RESOLUTION_DICT = {
    'daily': 'D',
    'weekly': 'W',
    'monthly': 'M',
    'quarterly': 'Q',
    'yearly': 'Y'
}


def random_characters(no_of_eng_char: int):
    return ''.join(random.choice(string.ascii_letters) for letter in range(no_of_eng_char))


def random_digits(no_of_digits: int):
    return random.randrange(start=pow(10, no_of_digits), stop=pow(10, no_of_digits+1))


def check_is_saved_db():
    pass


def generate_backtest_code(resolution: str, no_of_eng_char: int, no_of_digits: int,
                           instrument_code: str, direction: str, timeframe: str):
    if direction.upper() == 'LONG':
        direction = 'L'
    elif direction.upper() == 'SHORT':
        direction = 'S'
    else:
        print(f'[ERROR] The direction is invalid.')

    if RESOLUTION_DICT[resolution] is not None:
        return f'{RESOLUTION_DICT[resolution]}{random_characters(no_of_eng_char=no_of_eng_char)}' \
               f'{random_digits(no_of_digits=no_of_digits)}@{instrument_code}*{direction}{timeframe}'
    else:
        return f'{resolution}{random_characters(no_of_eng_char=no_of_eng_char)}' \
               f'{random_digits(no_of_digits=no_of_digits)}@{instrument_code}*{direction}{timeframe}'


def strategy_dict(strategy_code: str, ernest_ratio: float, sharpe_ratio: float, annual_return: float,
                  sortino_ratio: float, calmar_ratio: float, maxi_drawdown: float, frequency: int,
                  win_rate: float, time_to_maturity: int, description: str, last_backtest_time: int):
    return {
        'strategy_code': strategy_code,
        'ernest_ratio': ernest_ratio,
        'sharpe_ratio': sharpe_ratio,
        'annual_return': annual_return,
        'sortino_ratio': sortino_ratio,
        'calmar_ratio': calmar_ratio,
        'maxi_drawdown': maxi_drawdown,
        'frequency': frequency,
        'win_rate': win_rate,
        'time_to_maturity': time_to_maturity,
        'description': description,
        'last_backtest_time': last_backtest_time
    }


def strategy_template(description: str, stop_loss: str, entry_level: str, profit_level: str,
                      no_of_bars: int, action: str):
    if action != 'LONG' or action != 'SHORT':
        print('[ERROR] The action parameter is invalid.')

    return {
        'description': description,
        'stop_loss': stop_loss,
        'entry_level': entry_level,
        'profit_level': profit_level,
        'no_of_bars': no_of_bars,
        'action': action
    }


def fetch_signal_details(signal: pd.Series):
    return signal[signal == 1], signal[signal != 0]
