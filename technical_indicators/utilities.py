import pandas as pd


def check_sl_tl_position_pnl(direction, df, i, stop_loss, entry_point, profit_level, no_of_bars, ratio):
    if direction == 'LONG':
        target_level = entry_point + ratio * profit_level

        lists_of_highs = pd.Series(df[i + 1:i + no_of_bars + 1]['High'])
        lists_of_lows = pd.Series(df[i + 1:i + no_of_bars + 1]['Low'])
        print(df.index[i])
        lists_of_highs.reset_index(drop=True, inplace=True)
        lists_of_lows.reset_index(drop=True, inplace=True)

        high = lists_of_highs[lists_of_highs >= target_level]
        low = lists_of_lows[lists_of_lows < stop_loss]

        max_of_highs = max(lists_of_highs)
        min_of_lows = min(lists_of_lows)

        if target_level < max_of_highs:  # For successful cases, target_level must be met.

            if stop_loss <= min_of_lows:  # The first successful case, stop_loss is never be broken.
                print(f'[UPDATES] It is successful.')
                t = (True, target_level, high.index[0]+1, ratio * profit_level)
                return t

            elif high.index[0] < low.index[0]:
                # The second successful case, it meets the target_level first then break the stop_loss.
                print(f'[UPDATES] It is successful.')
                t = (True, target_level, high.index[0]+1, ratio * profit_level)
                return t

            else:
                print(f'[UPDATES] It hits the stop loss.')
                t = (False, target_level, low.index[0]+1, stop_loss - entry_point)
                return t

        else:
            print(f'[UPDATES] It never hits the target level.')
            t = (False, target_level, no_of_bars, df.iloc[(i + no_of_bars)]['Close'] - entry_point)
            return t

    elif direction == 'SHORT':
        target_level = entry_point - ratio * profit_level

        lists_of_highs = pd.Series(df[i + 1:i + no_of_bars + 1]['High'])
        lists_of_lows = pd.Series(df[i + 1:i + no_of_bars + 1]['Low'])

        high = lists_of_highs[lists_of_highs > stop_loss]
        low = lists_of_lows[lists_of_lows <= target_level]

        max_of_highs = max(lists_of_highs)
        min_of_lows = min(lists_of_lows)

        if target_level > min_of_lows:  # For successful cases, target_level must be met.

            if stop_loss >= max_of_highs:  # The first successful case, stop_loss is never be broken.
                print(f'[UPDATES] It is successful.')
                t = (True, target_level, low.index[0]+1, ratio * profit_level)
                return t

            elif high.index[0] > low.index[0]:
                # The second successful case, it meets the target_level first then break the stop_loss.
                print(f'[UPDATES] It is successful.')
                t = (True, target_level, low.index[0]+1, ratio * profit_level)
                return t

            else:
                print(f'[UPDATES] It hits the stop loss.')
                t = (False, target_level, high.index[0]+1, entry_point - stop_loss)
                return t

        else:
            print(f'[UPDATES] It never hits the target level.')
            t = (False, target_level, no_of_bars, df.loc[(i + no_of_bars), 'Close'] - entry_point)
            return t

    else:
        print(f'[ERROR] The direction parameter is invalid.')
        return None, None, None, None


def __calculate_position_pnl(self, direction, candle, i, stop_loss,
                             entry_point, profit_level, no_of_bars, ratio):
    if direction == 'LONG':
        success = False
        target_level = entry_point + ratio * profit_level
        pos = 1
        for nb in range(1, no_of_bars+1):
            self.__positions[i + nb] += pos

            if candle.l[i + nb] < stop_loss:
                self.__pnl_series[i + nb] += (stop_loss - candle.c[i + nb - 1])/candle.c[i + nb - 1]
                success = False
                print(f'[UPDATES] It hits the stop loss.')
                break
            elif candle.h[i + nb] >= target_level:
                self.__pnl_series[i + nb] += (target_level - candle.c[i + nb - 1])/candle.c[i + nb - 1]
                success = True
                print(f'[UPDATES] It is successful.')
                break
            else:
                self.__pnl_series[i + nb] += (candle.c[i + nb] - candle.c[i + nb - 1])/candle.c[i + nb - 1]

            if nb == no_of_bars:
                print(f'[UPDATES] It never hits the target level.')
                success = False

        return success

    elif direction == 'SHORT':
        success = False
        target_level = entry_point - ratio * profit_level
        pos = -1
        for nb in range(1, no_of_bars+1):
            self.__positions[i + nb] += pos

            if candle.h[i + nb] > stop_loss:
                self.__pnl_series[i + nb] += (candle.c[i + nb - 1] - stop_loss)/candle.c[i + nb - 1]
                success = False
                print(f'[UPDATES] It hits the stop loss.')
                break
            elif candle.l[i + nb] <= target_level:
                self.__pnl_series[i + nb] += (candle.c[i + nb - 1] - target_level)/candle.c[i + nb - 1]
                success = True
                print(f'[UPDATES] It is successful.')
                break
            else:
                self.__pnl_series[i + nb] += (candle.c[i + nb] - candle.c[i + nb - 1])/candle.c[i + nb - 1]

            if nb == no_of_bars:
                print(f'[UPDATES] It never hits the target level.')
                success = False

        return success

    else:
        print(f'[ERROR] The direction parameter is invalid.')

        return None
